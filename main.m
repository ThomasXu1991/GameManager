
#import <UIKit/UIKit.h>

#import "MyGameAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([MyGameAppDelegate class]));
    }
}
