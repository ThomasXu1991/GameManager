
#import "Sprite.h"

@interface Circle : Sprite {  
    int radius;
    float alpha;
} 

- (id) initWithRadius: (int) rds 
                  pos: (CGPoint) pxy;

@end