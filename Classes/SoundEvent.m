
#import "SoundEvent.h"

@implementation SoundEvent

- (id) initWithTime: (int) frt 
                pos: (CGPoint) pxy {

    if (self = [super init]) { 
        frameTime = frt;
        pos = pxy;
    }      
    return self;    
}

- (int) getTime {
    return frameTime;
}

- (CGPoint) getPos {
    return pos;
}

@end
