
@interface SoundEvent : NSObject {
    int frameTime;
    CGPoint pos;
}

- (id) initWithTime: (int) frt 
                pos: (CGPoint) pxy;

- (int) getTime;
- (CGPoint) getPos;

@end
