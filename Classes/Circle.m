
#import "Circle.h"
#import "GameManager.h"

@implementation Circle

- (id) initWithRadius: (int) rds 
                  pos: (CGPoint) pxy {     
    
    if (self = [super init]) { 
        radius = rds;
        pos = pxy;
        alpha = 1;
        active = true;
        
        NSString *sound = @"sound1.wav";
        int step = H/5; //5 sounds
        if (pos.y < step*1) {
            sound = @"sound1.wav";
        }
        else if (pos.y < step*2) {
            sound = @"sound2.wav";
        }
        else if (pos.y < step*3) {
            sound = @"sound3.wav";
        }
        else if (pos.y < step*4) {
            sound = @"sound4.wav";
        }
        else if (pos.y < step*5) {
            sound = @"sound5.wav";
        }
        [[GameManager getInstance] playSound: sound];
    }   
   
    return self;
}

- (void) draw {	 
    alpha -= 0.06;
    radius += 1;
    if (alpha <= 0) {
        active = false;
    }
    
    CGContextRef gc = UIGraphicsGetCurrentContext();
    CGContextSetRGBFillColor(gc, 0,1,0, alpha);    
    CGRect rect = CGRectMake (pos.x-radius, pos.y-radius, radius*2, radius*2); 	
	CGContextAddEllipseInRect(gc, rect);
    CGContextDrawPath(gc, kCGPathFill);
}

@end
