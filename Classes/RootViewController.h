//
//  RootViewController.h
//  GameManager
//
//  Created by XuLiang on 16/2/21.
//
//

#import <UIKit/UIKit.h>
#import "MainView.h"

@interface RootViewController : UIViewController

@property (strong, nonatomic) MainView *mainview;



@end
