
#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "RootViewController.h"

@interface MyGameAppDelegate : NSObject <UIApplicationDelegate> {
    id timer; //OS < 3.1 instanceOf NSTimer else instanceOf CADisplayLink   
}

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) RootViewController* rootviewController;

- (void) startGameLoop;
- (void) stopGameLoop;
- (void) loop;

@end

